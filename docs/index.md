# About

Our facility provides development, trainings, expertise and resources for bioinformatics,
primarily for academic research.

Provided resources include, but are not limited to, computing, biological
databanks, software and collaboration tools.

<span style="text-align: center; font-size: 1.3em; color: orange;">_**Click on the "Usage" menu at the top for help on using our services.**_</span>

<span style="text-align: center; font-size: 1.3em; color: red;">_**[Documentation on the data migration to new storage](new_storage.md)**_</span>

## Getting an account

To use most of our resources, you will need an account. Only a few of the web tools we host allow
anonymous usage.

To create an account, see [GenOuest account](usage/create_an_account.md).

## Using our resources

We host computing resources to submit jobs to a HPC cluster, a [Galaxy instance](https://galaxyproject.org/), a private cloud, preinstalled software and [databanks](usage/biomaj.md), and much more...
[See all our services](usage/services.md).

Each account gets a free amount of [data storage](usage/cluster.md#storage). If you have specific data storage requirements, we can discuss together to increase your quotas, however we expect you come with a clear data management plan (DMP).

If you plan to use our resources, please contact us _before_ submitting your research project proposal to funding agencies. This will allow us to plan your requirements together and, if needed, the purchase of computing resources.

## Trainings

You can ask to be a [_trainer_](usage/tps.md) to create temporary accounts
for your training session.

## Citing and terms of use

To use GenOuest services, you must comply with our [terms of use](policy/terms_of_use.md).

If you publish a research article, we ask you to add a note indicating that you used
our resources. Please see the [Citing GenOuest](policy/citing.md) page.

## GDPR and user data collection

All user information stored in the GenOuest information system is solely for
GenOuest use and is not shared with anyone else.

Personal information (email, phone, address etc.) is only used by administrators
to contact users in the following cases:

* communication (maintenance, etc.) via a mailing list. Users can unsubscribe
  from the mailing list at any time.
* online services can make use of a user's email address to send results per user request
* security issues
* service abuse

We do not use cookies to track your usage.
However, session cookies are sometimes used to enable you to log in to an application.
If you disable cookies in your browser, you won't be able to log in to those applications.
Once you logout, all session information is deleted.

## What we do

Managing a computing facility also means managing operating systems, networks,
user account and many other things. To do so, we internally develop a lot of
software. And because we use a lot of open source software, almost all our
developments are open source too. They are usually available at
[https://github.com/genouest](https://github.com/genouest).

We also have strong collaborations with other labs/entities such as [IFB](https://www.france-bioinformatique.fr/) (in particular the cluster and cloud actions) and the [Elixir](https://elixir-europe.org/) European infrastructure.

## FAQ

See our [FAQ](faq.md) for common questions/issues.

## Contact

Address:

```text
Plate-forme GenOuest IRISA-INRIA, Campus de Beaulieu 35042 Rennes cedex, France
```

Mail: [support@genouest.org](mailto:support@genouest.org)

Twitter: [@GenOuest](https://twitter.com/genouest)

Mastodon: [@genouest@mstdn.science](https://mstdn.science/@genouest)
