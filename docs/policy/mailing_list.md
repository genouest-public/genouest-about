# GenOuest mailing list

## About

Our mailing is a low rate list, and is used to send general information such as
maintenance and service issues.

Users are automatically subscribed at registration time.

## Unsubscribing

If for any reason you want to unsubscribe from our mailing list, each email
contains a link to do so.

You can also connect to the [GenOuest account manager](https://my.genouest.org)
to manually unsubscribe or resubscribe at any time.
