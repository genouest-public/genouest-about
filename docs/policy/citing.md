# Citing GenOuest

To cite GenOuest in your article, we suggest you include the following statement:

```text
We acknowledge the GenOuest bioinformatics core facility (https://www.genouest.org) for providing the computing infrastructure.
```

You can use this sentence as a template and modify it to better suit your needs. The key element is to mention the GenOuest bioinformatics facility and the website URL.

This will help us demonstrate the usefulness of the resources we provide, and as such, ensure the long term availability of theses resources.
