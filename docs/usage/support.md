# Support

## Before submitting an issue

* Did you check our [help page](https://help.genouest.org) ?
* Did you check our [FAQ](https://help.genouest.org/faq/) ?

## Submitting an issue

When reporting a problem, please try to describe it as precisely as possible.
For instance, by giving the following information:

* Use a meaningful subject in your mail/bug report.
* Give a summary of your problem, do not just paste commands and commands output (but do paste them though!).
* Include the names of any web sites involved.
* Include the names of any nodes involved.
* Include date and time (somebody could use that to search the logs).
* If relevant, include slurm job id, queue, job requirements, ...
* Include the exact commands you typed.
* Include the exact output of the commands you typed.

Explain exactly what "does not work". Explain what you expected to happen.

Give some possible causes (what have you changed recently?) if you have any guess.

In general, give as much information as possible.
If we have to complete that information, it will be a loss of time.
