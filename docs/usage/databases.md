# Mysql databases

You can create a personal MySQL database from your personal space on [https://my.genouest.org](https://my.genouest.org), in the “Databases” block.

Once it is created, you will receive an email with credentials to connect to the database. Note that you can only connect from the GenOuest network (`genossh.genouest.org` or the compute nodes, for example).

To test the connection, you can run the command line MySQL client like this:

```bash
mysql -u <username> -h <host> -p <database>
```

Then enter the password you received in the email.
