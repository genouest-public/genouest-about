# Our services

## HPC cluster: Slurm

Our batch computing system is based on slurm.

From our bastion server `genossh.genouest.org`, you can execute computing jobs or connect interactivly to one of the computing nodes.

See the [Cluster](cluster.md) and [Slurm](slurm.md) pages

## Openstack cloud

Private cloud to launch some virtual machines. You are the owner of the VM
(root). There is an expiration mechanism. If you do not extend the lifetime
of the VM (you will receive a reminder email), the VM is deleted.
Along VM you can attach additional disks or use a shared disk (manilla)
among them.

* [Openstack dashboard](https://genostack.genouest.org)
* [Usage](cloud.md)

## Databases

[my](https://my.genouest.org) self-service dashboard lets you create
on-demand MySQL databases. Choose a database name and a database is
automatically created. An email containing its credentials is sent once created.

Databases are hosted on our database server (genobdd)

## Cesgo

[CeSGO](https://cesgo.org) provides an integrated environment to help
scientists to work from project ideas to publication through data
production and management.

The CeSGO project is offered by the GenOuest core facility and is a part of
CPER, funded by European funds, by the state and by the Brittany region.

Among the different features you have access to a chat collaboration tool,
a project management tool (Kanboard using Kanban), a collaboration system
to share document/progress/info with your team (public or private), and a
file sharing tool based on Owncloud (like Dropbox).

All of your data is hosted on GenOuest resources (France) and remain private.
