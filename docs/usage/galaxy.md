# Galaxy

[Galaxy](https://galaxyproject.org/) is a web portal allowing to execute bioinformatics analysis in a user-friendly environment.

GenOuest is an active contributor to the Galaxy ecosystem, and participates in different international projects involving Galaxy (e.g. [Gallantries](https://gallantries.github.io/), [EuroScienceGateway](https://galaxyproject.org/projects/esg/), [UseGalaxy.eu](https://usegalaxy.eu/)).

GenOuest used to operate two (very similar) Galaxy servers until the end of 2023:

- [GenOuest instance (galaxy.genouest.org)](https://galaxy.genouest.org/)
- [BIPAA instance (bipaa-galaxy.genouest.org)](https://bipaa-galaxy.genouest.org/)

In 2023, we decided to shut down these two Galaxy servers, and to focus our efforts on the French national Galaxy server: [UseGalaxy.fr](https://usegalaxy.fr). While you will access Galaxy at a different address, keep in mind that we will still be working on it behind the scenes, with other IFB contributors.

Here are the main reasons for this migration:

- UseGalaxy.fr is supported by the [IFB (Institut Française de Bioinformatique)](https://www.france-bioinformatique.fr/). As GenOuest is a member platform of the IFB, and just as we already participate to numerous other IFB projects, it feels natural to join this federating initiative
- We are actually active contributors to the national [UseGalaxy.fr](https://usegalaxy.fr) server since 2020
- Maintaining a Galaxy server takes significant time and resources. As UseGalaxy.fr is becoming more mature and robust, we felt like maintaining our two local servers was an unneeded duplication of efforts
- While our instances used to have some very specific features (access to storage from the GenOuest cluster, specific tools and workflows), most of these features are now available on any other Galaxy server, including UseGalaxy.fr

## UseGalaxy.fr account

If you plan on using UseGalaxy.fr, you will first need to have an account. GenOuest and UseGalaxy.fr are not the same: you cannot use your your GenOuest account to login to UseGalaxy.fr (and vice versa).

You can easily create a new account by clicking on the [Login or Register](https://usegalaxy.fr/login) link at the top of Galaxy's welcome page, then clicking on "Don't have an account? Register here".

If you prefer, you can also use your institution account, by clicking on the "Elixir LOGIN" button on this same page.

## Managing your data in Galaxy

From any Galaxy server, you can easily export an entire history into a single file (tar.gz archive, in a standard format). This file will contain each dataset from your history, as individual files, and all the metadata concerning each dataset. You can store this file on the storage of your choice for future use. And you can also at any time import this archive back into a new history on any Galaxy server and continue working on it as usual.

With the following instructions, you should be able to:

- export or import complete histories from any Galaxy server
- access any data from your GenOuest cluster account using UseGalaxy.fr
- push any data from UseGalaxy.fr to your GenOuest cluster account

### How to download a history

Click on the history menu, and select **"Export History to File"**:

![The history menu where you can see the "Export History to File" option](../img/galaxy/history_menu_export.png)

You should see a page looking like this:

![History export page](../img/galaxy/history_export_file_1.png)

Click on the **"Click here to generate a new archive for this history."** link. This will launch a job on the compute cluster to generate the file containing all of the history data.

Wait a moment (depending on the size of the history), and when the file is ready, a link will appear, from which you will be able to download the archive containing your history (beware, the file can be big depending on your history size).

![History download link](../img/galaxy/history_export_file_2.png)

Generating the file can take some time for big histories, you can safely disconnect and come back later to this page to check if the download link is ready (just click back on the history menu, and select **"Export History to File"**).

### How to export a history to the GenOuest cluster {: #export_remote }

Before doing this, you need to fill out your GenOuest account credentials in your Galaxy user preferences, follow the instructions [to access storage spaces from the GenOuest cluster in Galaxy](#remote_cluster) below to learn how to do it. <!-- markdownlint-disable-line MD051 -->

Click on history menu, and select **"Export History to File"**:

![History menu where you can see the "Export History to File" option](../img/galaxy/history_menu_export.png)

You should see a page looking like this:

![History export page](../img/galaxy/history_export_file_1.png)

Click on "to a remote file" on the left, then click on **Click to select directory**.

Now click on **GenOuest FTP server**:

![GenOuest FTP server link](../img/galaxy/remote_1.png)

And now you need to select a directory where the exported history will be deposited by Galaxy.

Choose a name for the file that will be created, and click on **Export**.

![Export form](../img/galaxy/export_remote.png)

Generating the file can take some time for big histories, the file will appear on the GenOuest cluster when it's finished.

### How to upload a history to UseGalaxy.fr

If you don't have a GenOuest account, the easiest is to export the history, download it to your hard drive, then import it to UseGalaxy.fr:

Go to your histories list:

![Histories list menu](../img/galaxy/import_1.png)

Click on the **Import history** button at the top right. Choose **Upload local file from your computer** and select the history file you have previously downloaded.

![Import from a local file](../img/galaxy/import_2.png)

Click on **Import history** and wait for the file to be uploaded and then extracted as a new history.

Note that with this method, you might get an error when trying to transfer big histories. This is a known limitation that should be fixed some time soon. Prefer the next method (using the GenOuest cluster) if you have this problem, and if it does not work, contact us if this is blocking you.

### How to access storage spaces from the GenOuest cluster in Galaxy {: #remote_cluster }

If you have a [GenOuest account](https://my.genouest.org), you can access your GenOuest cluster storage volumes directly from galaxy.genouest.org, bipaa-galaxy.genouest.org or UseGalaxy.fr.

This allows you to transfer files directly between the cluster and Galaxy, without needing to download it on your local computer first, making the transfer much faster.

To enable this, you need to enter your GenOuest account login and password in your Galaxy user preferences (do it only once the first time you want to make use of it).

Click on the **User** menu at the top, then on **Preferences**:

![User preferences menu](../img/galaxy/user_prefs.png)

Then click on **Manage information**:

![Manage information menu](../img/galaxy/manage_info.png)

And scroll down to the **Your GenOuest/CeSGO account**. There you need to write your GenOuest account login and password.

![GenOuest credentials](../img/galaxy/genouest_account.png)

And click on the **Save** button at the bottom of the page.

You're done! Now you should be able to import files from the cluster into Galaxy.

Click on the **Upload Data** button in Galaxy, and then click on **Choose remote files**:

![Upload box with the remote files button](../img/galaxy/upload_remote.png)

Now click on **GenOuest FTP server**:

![GenOuest FTP server link](../img/galaxy/remote_1.png)

Now you can navigate in the cluster storage and select the files that you want to import into Galaxy:

![Navigation in the cluster storage](../img/galaxy/remote_2.png)

Use the checkboxes to select the desired files. Note that due to technical reasons, `/scratch` is currently not accessible via FTP.

This method uses the same permission system as on the cluster, so you will only be able to access files that you would be able to access on the cluster.

### How to ask for a higher quota on UseGalaxy.fr

The best place to ask for this is on the [UseGalaxy.fr forum](https://community.france-bioinformatique.fr/c/galaxy/8).

The default quota is 100Gb, but we can give you more if you need. Please be reasonable: if you have some finished analyses in Galaxy, the best is probably to export it out of Galaxy to save space in your quota. You will always have the possibility to reimport it later if you need to access it or make new analyses on it.

### How to install missing tools or to get help on UseGalaxy.fr

The best place to ask for this is on the [UseGalaxy.fr forum](https://community.france-bioinformatique.fr/c/galaxy/8).
