# Training accounts

## Be a trainer

As a trainer, you can request the creation of temporary
accounts for a defined duration.

To be a trainer, send an email to [support@genouest.org](mailto:support@genouest.org)
requesting training rights and explaining your needs.

After approval, you will have access in [my](https://my.genouest.org) to a *TP reservations* menu.

## Create sessions

To create a training session, specify the number of accounts
needed and the begin/end session dates.

Accounts will have access to all GenOuest services, with a home directory, like any other account,
they just don't need a valid email and won't be subscribed
to the mailing list.

Accounts are created 5 days before the session, and an email
containing user identifiers and passwords will be sent to the
trainer.

At session time, or once accounts are created, trainees *may* need
to create/upload their SSH key ([my](https://my.genouest.org)),
depending on needed materials/services.

Accounts and related storage are deleted 5 days after the session,
this delay gives time for trainees to get some training session
data if needed.
