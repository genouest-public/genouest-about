# Workflows

## Nextflow

Nextflow is installed on the cluster, it can be used by sourcing the correct environnement:

```bash
. /local/env/envnextflow-23.10.0.sh
```

nf-core is also preinstalled, allowing you to use standard workflows from [nf-co.re](https://nf-co.re/) using the [nf-core client](https://nf-co.re/tools#listing-pipelines).

Nextflow is preconfigured to use Singularity (when workflows are designed to use containers) and to distribute jobs on the Slurm cluster.

A GenOuest *profile* is available to make sure that the scratch, db, and groups folders are mounted in your singularity containers. You can select it by using the `-profile genouest` option.

## SnakeMake

SnakeMake is pre-installed on the cluster, you can source it like so:

```bash
. /local/env/envsnakemake-8.0.1.sh
```
