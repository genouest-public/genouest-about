# Hosting a service

## Hosting scientific webtools

On a case-by-case basis, GenOuest might host a scientific webtool for the community's needs (see this [list](https://www.genouest.org/hosted-resources-and-tools/)).
If you want GenOuest to host a tool on your behalf, you will have to fill in a _Hosted Scientific Service Management Plan_[^1] (HSSMP) on the GenOuest DSW instance.

[^1]: The HSSMP aims to gather and centralize essential information about the webtool, and ensure its supervisor follow-up.

To this end, please **first** log in to [dsw.genouest.org](https://dsw.genouest.org) using your GenOuest credentials (`GenOuest` button) and **then** open a ticket with the support to ask for a HSSMP.

The support will give you access to a fresh new HSSMP which you can find in your DSW space, under `Projects` tab.

### Nota bene

* The HSSMP should always be up-to-date. You can update it whenever you need, but it should be reviewed at least every year.
* It is an auto-saving document.
