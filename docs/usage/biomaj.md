# BioMAJ

[BioMAJ](https://biomaj.genouest.org/) is our workflow engine in charge of automatically updating the reference databanks.
The databanks are available in the /db directory.

They are automatically updated and indexed on a regular basis by BioMAJ.
Some banks are available in multiple versions. The `current` symbolic link targets
the latest version available on our servers.

You can consult the list of available banks on our
[BioMAJ instance](https://banks.genouest.org/app/#/bank).

When working on /db, do not use the /db/..../current path in your scripts,
always look at the targeted directory to use its real path as banks may be updated
during your job. You can get a real path with:

    readlink -f /db/genbankRelease/current/

If you need a databank that is not available in `/db`, just send an email to [support@genouest.org](mailto:support@genouest.org),
and we will help you.
